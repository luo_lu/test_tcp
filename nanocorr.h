#ifndef __NANOCORR_H
#define __NANOCORR_H


#define chuan_kong_su_lv_max 50000
#define chuan_kong_su_lv_min 0.1
#define ying_bao_you_xiao_du_max 149.9
#define ying_bao_you_xiao_du_min -9.9
#define ping_jun_su_lv_max 50000
#define ping_jun_su_lv_min 0.1
#define chuan_kong_shen_du_max 16000
#define chuan_kong_shen_du_min 0.001
#define tan_tou_dian_wei_max 4.999
#define tan_tou_dian_wei_min -5
#define tan_tou_ping_jun_dian_liu_max 4999.999
#define tan_tou_ping_jun_dian_liu_min -5000
#define E_off_max 19.999
#define E_off_min -20
#define E_on_max 19.999
#define E_on_min -20
#define E_corr_max 4.999
#define E_corr_min -5
#define V_rms_max 20
#define V_rms_min 0.001
#define I_rms_max 400
#define I_rms_min 0.01
#define I_dc_max 199
#define I_dc_min -200
#pragma pack(1)
typedef struct{
    unsigned char chuan_kong_su_lv[3];
    unsigned char ying_bao_you_xiao_du[2];
    unsigned char ping_jun_su_lv[3];
    unsigned char chuan_kong_shen_du[3];
    unsigned char tan_tou_dian_wei[2];
    unsigned char tan_tou_ping_jun_dian_liu[3];
}NANO_PROBE;

typedef struct{
    float chuan_kong_su_lv;
    float ying_bao_you_xiao_du;
    float ping_jun_su_lv;
    float chuan_kong_shen_du;
    float tan_tou_dian_wei;
    float tan_tou_ping_jun_dian_liu;
}NANO_PROBE_FLOAT;

typedef struct{
    unsigned char addr[2];
    unsigned char func;
    unsigned char msg_len;
}NANO_MSG_HEAD;

typedef struct{
    unsigned char bcd_time[7];
    unsigned char probe_cnt;
}NANO_MSG_TIME_CNT;

typedef struct{
    unsigned char E_off[2];
    unsigned char E_on[2];
    unsigned char E_corr[2];
    unsigned char V_rms[2];
    unsigned char I_rms[2];
    unsigned char I_dc[3];
}NANO_CP_PARAM;
typedef struct{
    float E_off;
    float E_on;
    float E_corr;
    float V_rms;
    float I_rms;
    float I_dc;
}NANO_CP_PARAM_FLOAT;

typedef struct{
    NANO_MSG_HEAD nano_msg_head;
    NANO_MSG_TIME_CNT nano_msg_time_cnt;
    NANO_PROBE nano_probes;
    NANO_CP_PARAM nano_cp_param;
}NANO_FRAME;
#pragma pack()
#define ADDR_H '9'
#define ADDR_L '9'
#define READ_NANO '1'

char check_nano_format(unsigned char *buf,unsigned char len,NANO_FRAME **p2);
#endif
