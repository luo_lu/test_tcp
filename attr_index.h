#ifndef __ATTR_INDEX_H
#define __ATTR_INDEX_H

    #define chuan_kong_su_lv_attr 0x10
    #define ying_bao_you_xiao_du_attr 0x11
    #define ping_jun_su_lv_attr 0x12
    #define chuan_kong_shen_du_attr 0x13
    #define tan_tou_dian_wei_attr 0x14
    #define tan_tou_ping_jun_dian_liu_attr 0x15
    #define E_off_attr 0x16
    #define E_on_attr 0x17
    #define E_corr_attr 0x18
    #define V_rms_attr 0x19
    #define I_rms_attr 0x1a
    #define I_dc_attr 0x1b

#endif
