
#include <stdlib.h>
#include <stdio.h>
#include < string.h >
#include "crc32.h"
#include "attr_index.h"
#include "nanocorr.h"
#define PACKET_DATA 0X00
#define PACKET_CMD 0X01
#define PACKET_ATTR 0X02
#define PACKET_HEART 0X03
#define PACKET_ACK 0X04

#define HEAD1 0X9C
#define HEAD2 0X25
#define HEAD3 0X6A
#define HEAD4 0XAC
// #pragma comment (lib, "Mswsock.lib")

unsigned char packet_buf[1024];
#pragma pack (1)
typedef struct {
    unsigned char head[4];
    unsigned short mlen;
    unsigned char ptype;
    unsigned short id;
    unsigned char seq;
}PACKET;
#pragma pack ()

unsigned short sum16(unsigned char* buf, unsigned char len)
{
    unsigned char i;
    unsigned short s = 0;
    for (i = 0; i < len; i++)
        s += buf[i];
    return s;
}
float chuan_kong_su_lv = chuan_kong_su_lv_min;
float ying_bao_you_xiao_du = ying_bao_you_xiao_du_min;
float ping_jun_su_lv = ping_jun_su_lv_min;
float chuan_kong_shen_du = chuan_kong_shen_du_min;
float tan_tou_dian_wei = tan_tou_dian_wei_min;
float tan_tou_ping_jun_dian_liu = tan_tou_ping_jun_dian_liu_min;
float E_off = E_off_min;
float E_on = E_on_min;
float E_corr = E_corr_min;
float V_rms = V_rms_min;
float I_rms = I_rms_min;
float I_dc = I_rms_min;
int fill_packet_payload(void)
{
    int i;
    int offset = sizeof(PACKET);
    unsigned char atti = 1;

    packet_buf[offset] = 0x7;
    offset += 1;
    packet_buf[offset] = atti;
    offset += 1;

    packet_buf[offset] = ying_bao_you_xiao_du_attr;
    offset += 1;
    memcpy(packet_buf + offset, &ying_bao_you_xiao_du, 4);
    ying_bao_you_xiao_du += 1.0;
    if (ying_bao_you_xiao_du > ying_bao_you_xiao_du_max)
        ying_bao_you_xiao_du = ying_bao_you_xiao_du_min;

    offset += 4;

    return atti * 5 + 2;
}
int fill_cmd_ack_payload(void)
{
    int offset = sizeof(PACKET);

    memcpy(packet_buf + offset, "OK", 2);

    return 2;
}
int fill_packet_crc(int offset)
{
    unsigned short c;

    PACKET* p = (PACKET*)packet_buf;

    c = sum16(&p->ptype, offset - 6);
    memcpy(packet_buf + offset, &c, 2);
    return 0;
}
#define PACKET_DATA 0X00
#define PACKET_CMD 0X01
#define PACKET_ATTR 0X02
#define PACKET_HEART 0X03
#define PACKET_ACK 0X04
#define PACKET_CMD_ACK 0X05

#define HEAD1 0X9C
#define HEAD2 0X25
#define HEAD3 0X6A
#define HEAD4 0XAC

#define PTYPE2CRC_SIZE 8
#define HEAD_LEN_SIZE 6
#define MIN_NET_SIZE 12
char check_packet_format(unsigned char* buf, unsigned short len)
{
    PACKET* p = (PACKET*)buf;
    unsigned short c, c2;

    if (len < MIN_NET_SIZE) {
        printf("len too short %d", len);
        return 0;
    }

    if (p->head[0] != HEAD1 || p->head[1] != HEAD2 ||
        p->head[2] != HEAD3 || p->head[3] != HEAD4) {
        printf("head error");
        return 0;
    }

    if (p->mlen + HEAD_LEN_SIZE != len) {
        printf("len error %d %d", p->mlen, len);
        return 0;
    }

    c = sum16(&p->ptype, p->mlen - 2);
    c2 = *((unsigned short*)(buf + len - 2));
    if (c != c2) {
        printf("crc error %08x %08x", c, c2);
        return 0;
    }
    return 1;
}

int mkup_packet(unsigned int id, unsigned char seq, unsigned char type)
{
    int len = 0;

    PACKET* p = (PACKET*)packet_buf;
    p->head[0] = HEAD1; p->head[1] = HEAD2; p->head[2] = HEAD3; p->head[3] = HEAD4;
    if (type == PACKET_DATA)
        len = fill_packet_payload();
    else if (type == PACKET_CMD_ACK) {
        len = fill_cmd_ack_payload();
    }
    p->mlen = len + 6;
    p->ptype = type;
    p->id = id;
    p->seq = seq;
    fill_packet_crc(len + sizeof(PACKET));
    return len + sizeof(PACKET) + 2;
}

int gen_sim_data(unsigned char **buf)
{
    int len;
    static unsigned char seq = 126;

    len = mkup_packet(0x0337, seq, 0x00);
    *buf =(unsigned char *) &packet_buf;
    seq++;
    if (seq > 130)
        seq = 126;
    return len;
}
int  process_packet(unsigned char* buf, unsigned short len, unsigned char** ack_buf)
{
    char ret = check_packet_format(buf, len);
    if (!ret) {
        return -1;
    }
    PACKET* p = (PACKET*)buf;
    if (p->ptype == PACKET_DATA) {
        printf("recv data,id=%d seq=%d\n", p->id, p->seq);
        int len = mkup_packet(p->id, p->seq, PACKET_CMD_ACK);
        *ack_buf = (unsigned char*)&packet_buf;
        return len;
    }
    return -1;
}
void dump_hex(unsigned char* buf, int len)
{
    int i;

    for (i = 0; i < len; i++) {
        printf("%02x ", buf[i]);
        if (i > 0 && i % 32 == 0) {
            printf("\n");
        }
    }
    printf("\n");
}